# clone
cd /var
rm -rf www
git clone https://gitlab.com/i3-public/lfn.git
mv lfn www

wget -O /etc/nginx/sites-available/default https://gitlab.com/i3-public/lfn/-/raw/main/conf/default
service nginx reload

# tmp
rm -rf /var/www/html/tmp
mkdir /var/www/html/tmp
chmod 0777 /var/www/html/tmp

# ffmpeg
apt -y install ffmpeg

# cron
wget -qO- https://gitlab.com/i3-public/lfn/-/raw/main/conf/cron.txt > /var/spool/cron/crontabs/root

# monitor
wget https://gitlab.com/i3-public/net-usage/-/raw/master/README.md -O ~/net-usage-installer.sh
bash ~/net-usage-installer.sh 4310 skipserver


# main port: 4310
# wget -qO- https://gitlab.com/i3-public/lfn/-/raw/main/README.txt | bash

wget -O dockerfile https://gitlab.com/i3-public/conf/-/raw/main/nginx-on-docker/conf/dockerfile
sed -i 's,#INJECT,RUN wget -qO- https://gitlab.com/i3-public/lfn/-/raw/main/conf/patch.sh | sh,g' dockerfile

docker rm -f lfn
docker rmi lfn-image

docker build -t lfn-image -f dockerfile .
docker run -t -d --restart unless-stopped --name lfn -p 4310:80 -p 4311:443 lfn-image

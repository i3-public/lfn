<?php

# 2022-11-01

chdir('..');

ini_set('display_errors', 'On');
error_reporting(E_ALL & ~E_NOTICE);
$reserved_files = [ 'dotenv' ];


foreach( $reserved_files as $file ){
	$file = 'inc/'.$file.'.php';
	if( file_exists($file) ){
		include_once($file);
	}
}


foreach( glob('inc/*.php') as $file ){
	if(! in_array($file, get_included_files()) ){
		include_once($file);
	}
}

chdir('html');


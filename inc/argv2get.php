<?php


function argv2get(){
	
	if( $_SERVER['argc'] ){
	
		for( $i=1; $i < sizeof( $_SERVER['argv'] ); $i+=2 ){
		    
		    $key = $_SERVER['argv'][$i];
		    $val = $_SERVER['argv'][$i+1];

		    if( substr( $key, 0, 2) == '--' ){
		    	$_GET[ substr($key, 2) ] = $val;
		    	$_REQUEST[ substr($key, 2) ] = $val;
		    }

		}

	}

}

argv2get();


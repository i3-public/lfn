<?php


function watermark_make( $photo, $watermark, $save_to=null, $location='bottom-right' ){

	// Load the stamp and the photo to apply the watermark to
	$im = imagecreatefrom($photo);
	$watermark = imagecreatefrom($watermark);

	// Set the margins for the stamp and get the height/width of the stamp image
	$bordermargin = 20;
	switch ($location) {

		case 'top-left':
		case 'left-top':
			$water_x = $bordermargin;
			$water_y = $bordermargin;
			break;
		
		case 'top-right':
		case 'right-top':
			$water_x = imagesx($im) - imagesx($watermark) - $bordermargin;
			$water_y = $bordermargin;
			break;

		case 'bottom-left':
		case 'left-bottom':
			$water_x = $bordermargin;
			$water_y = imagesy($im) - imagesy($watermark) - $bordermargin;
			break;

		case 'center-center':
			$water_x = (imagesx($im) / 2) - (imagesx($watermark) / 2);
			$water_y = (imagesy($im) / 2) - (imagesy($watermark) / 2);
			break;

		case 'bottom-right':
		case 'right-bottom':
		default:
			$water_x = imagesx($im) - imagesx($watermark) - $bordermargin;
			$water_y = imagesy($im) - imagesy($watermark) - $bordermargin;
			break;
	}

	// echo $location."<br>";
	// echo $water_x." - ".$water_y;
	// die();

	// Copy the stamp image onto our photo using the margin offsets and the photo 
	// width to calculate positioning of the stamp. 
	imagecopy($im, $watermark, 
		$water_x, 
		$water_y, 
		0, 0, imagesx($watermark), imagesy($watermark) );

	// Output and free memory
	if(! $save_to ){
		header('Content-type: image/png');
	}
	imagejpeg($im, $save_to, 35);
	imagedestroy($im);

	return $save_to;

}


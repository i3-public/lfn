<?php

# tiny : [ 1 , 720 , 480:270 ]

function ffmpeg_fetch_frame( $source, $timeout, $skipwatermark=0, $tiny=0 ){

	$arr_of_vod_extn = [ 'avi', 'mkv', 'flv', 'mp4', 'mov' ];

	if( in_array( strtolower(substr( strrchr($source, '.') , 1 )), $arr_of_vod_extn ) ){
		$is_vod = 1;
	} else {
		$is_vod = 0;
	}

	$frame = "tmp/".md5($source).".".( $_GET['quality'] == 'high' ? 'png' : 'jpg' );
	@unlink($frame);
	
	if( $_GET['quality'] == 'high' ){
	    $qu = " -qscale:v 2 ";
	}
	
	shell_exec(" timeout $timeout ffmpeg -user-agent \"96180ef8e8512f7fd17f84ef1f683c88\" -y -i \"$source\" $qu ".( $is_vod ? ' -ss 00:00:00.001 ' : '') ." -vframes 1 -q:v 20 $frame ");

	if(! file_exists($frame) ){
		return false;
	
	} else {

		if(! $skipwatermark ){

			list($width, $height) = getimagesize($frame);

			if( $height < 600 ){
				$watermark = "image/480.jpg";
			} else if( $height < 800 ){
				$watermark = "image/720.jpg";
			} else if( $height < 1300 ){
				$watermark = "image/1080.jpg";
			} else if( $height < 1600 ){
				$watermark = "image/1440.jpg";
			} else {
				$watermark = "image/2160.jpg";
			}

			$frame = watermark_make($frame, $watermark, $save_to=$frame, $location='center-center');

		}

		if( $tiny ){
			if( $tiny == 1 ){
				$tiny = "480:270";
			} else if( is_numeric($tiny) ) {
				$tiny = $tiny.":".( $tiny * 9 / 16 );
			}
			shell_exec(" ffmpeg -user-agent \"96180ef8e8512f7fd17f84ef1f683c88\" -i $frame -vf scale=$tiny $frame -y ");
		}

		return $frame;

	}

}



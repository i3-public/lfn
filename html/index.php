<?php 

include_once('../inc/.php');



# 
# this server wont update any database
# and, just wil check and reply
# it needs some agent to use it as a node
#


switch( $_GET['do'] ){
    case 'current_process':
        echo trim( shell_exec(" ps aux | grep ffmpeg | grep -v grep | grep -v 'sh -c'  | grep timeout | wc -l ") , " \r\n\t" );
        die();
}


#
# flags
if(! $timeout = @intval($_REQUEST['timeout']) ){
    $timeout = 20;
}

if(! $screenshot = @intval($_REQUEST['screenshot']) ){
    $skipwatermark = 1;
} else {
    $skipwatermark = @intval($_REQUEST['skipwatermark']);
    $tiny = @intval($_REQUEST['tiny']);
}
#



#
# single request
if( $source = trim($_REQUEST['source']) ){
    
    if( is_numeric($source) ){
        // $source = "http://".main_ip.':8080/'.test_acc.'/'.$source;
        echo 'not supported';
        die;
        
    } else if( substr($source, 0 , 4) == 'http' ){
        $source = rawurldecode($source);

    } else {
        $source = text_decompress($source);
    }

    $frame = ffmpeg_fetch_frame( $source, $timeout, $skipwatermark, $tiny );

    #
    # if its an internal request
    if( $name = @trim($_REQUEST['name']) ){
        
        if( $frame ){
            echo "#EXTINF:-1,".$name."<br>";
            echo $source."<br>";
        }
        
        if( co_process($_REQUEST['psign']) <= 1 ){
            // echo "<meta charset=\"UTF-8\">";
            echo "<style>* { color: green; }</style><!-- DONE -->";
        }

    #
    # if its a screenshot request
    } else if( $screenshot ){
        
        if( $frame ){
            // header('Content-Description: File Transfer');
            header('Content-type: '.mime_content_type($frame));
            // header('Content-Disposition: attachment; filename="'.basename($frame).'"');
            header('Expires: 0');
            // header('Cache-Control: must-revalidate');
            // header('Pragma: public');
            header('Content-Length: ' . filesize($frame));
            readfile($frame);
            // header('Location: ./'.$frame, true, 302);
        
        } else {
            header('Location: ./image/blank.jpg', true, 302);
        }

    #
    # if its a filter request
    } else {

        if( $frame ){
            echo "OK";

        } else {
            echo "ER";
        }

    }



#
# multiple request - all in one
} else if( $source_mix = $_POST['source_mix'] ) { // format is json
    
    $filename = 'tmp/'.md5($source_mix).'.htm';    
    $link = "http://".$_SERVER['SERVER_ADDR']."/".$filename;
    echo "<hr><a href=\"$link\" style=\"text-decoration: none; color: royalblue;\" target=\"_blank\" >$link</a><br>Please wait until the color change to <font color=green >green</font>.";

    ffmpeg_multiple_fetch_frame( $source_mix, $timeout, $filename );



#
# help
} else {
    
    echo "something wrong !<br><br>";
    
    // echo "http://lfn.i3ns.net/?timeout=4&screenshot=1&skipwatermark=1&source=eJzLKCkpsNLXT8wpyEhMyc%2FXS0nJK9bLSy2xsjAwMNB3DrY0MC1ILEnOyDc2MDQwtNTP9KrKyrUIKykK0jc1NjKwAAC0ZhQp<br>";
    // echo "http://lfn.i3ns.net/?timeout=4&screenshot=1&skipwatermark=1&source=http%3A%2F%2Falphadoo.ddns.net%3A8000%2FCS905patcho301019%2FiJzjm8VtrR%2F53208<br>";
    
}

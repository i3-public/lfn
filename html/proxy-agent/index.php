<?php

if(! $timeout = intval($_GET['timeout']) ){
    $timeout = 20;
}

if( $url = urldecode( $_GET['url'] ) ){

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	// curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36');
	curl_setopt($ch, CURLOPT_USERAGENT, '96180ef8e8512f7fd17f84ef1f683c88');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch,CURLOPT_TIMEOUT,1000 * $timeout);
	echo curl_exec($ch);
	curl_close($ch);

}



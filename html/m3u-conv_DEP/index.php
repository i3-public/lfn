<?php

if(! $u = $_GET['u'] ){
	echo "no source defined";

} else if(! $m3u = base64_decode( str_replace("_", "=", $u) ) ){
	echo "not base64";

} else if(! $m3u = file_get_contents($m3u) ){
	echo "cant fetch m3u";

} else {

	$line_s = explode( "\n", $m3u );
	
	foreach( $line_s as $i => $line ){
		if( strstr($line, '",') ){
			$line_s[$i] = '#EXTINF:-1,' . trim( explode('",', $line)[1] );
		}
	}
	$m3u = implode("\n", $line_s);


	header("Content-Type: audio/mpegurl;");
	header("Content-Disposition: inline; filename=\"list.m3u\"");
	echo $m3u;

}

